const express = require('express');
const app = express();
const bcrypt = require('bcrypt')
var bodyParser = require('body-parser')
const database = require('./database/database');
var CryptoJS = require("crypto-js");
const axios = require('axios');

let User = require('./database/models/user');

var corsOptions = function(req, res, next){ 
    res.header('Access-Control-Allow-Origin', '*'); 
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 
    'Content-Type, Authorization, Content-Length, X-Requested-With');
     next();
}

app.use(corsOptions) 
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))


app.use('/api/user/login', async (req, res) => {

    const obj = JSON.parse(JSON.stringify(req.body));
    const userExists = await database.findUserByUsername(obj.username, "users");


    var bytes  = CryptoJS.AES.decrypt(obj.password, 'budifgbbesiufbeugahuf7yu76yty874y4hr7fdyg3huey7y');
    var originalPass = bytes.toString(CryptoJS.enc.Utf8);

    if (userExists) {
        console.log("User exists, checking password");
        const userPassword = await database.getUserPassword(obj.username, "users");
        const passwordMatch = await comparePasswords(originalPass, userPassword);
        if (passwordMatch) {
            console.log("Password match");
            res.status(200);
            res.send({
                token: await genToken(obj.username, originalPass)
            })
        } else {
            console.log("Password does not match");
            res.status(401)
            res.send("Password does not match")
        }
        
    } else {
        console.log("User does not exist");
        res.status(401)
        res.send("User does not exist")
    }  

});


app.post('/api/user/register', async (req, res) => {

    res.header("Access-Control-Allow-Origin", "*");

    const obj = JSON.parse(JSON.stringify(req.body));

    // console.log(obj);

    const user = new User({
        email: obj.email,
        username: obj.username,
        password: obj.password
    });

    user.save()
    .then((doc) => {
      console.log(doc);
      res.status(201).json(doc);
    })
    .catch((err) => {
      console.error(err);
        res.status(500).json(err);  
    });
    
});

app.use('/api/username', async (req1, res1) => {
    let obj = JSON.parse(JSON.stringify(req1.body));
    console.log(obj);
    console.log("Checking if username " + obj.username + " exists");
    const userExists = await database.findUserByUsername(obj.username, "users");
    if (userExists) {
        res1.status(200);
        res1.send({
            exists: true
        })
    } else {
        res1.status(200);
        res1.send({
            exists: false
        })
    }

});

app.delete('/api/user/delete', async (req, res) => {
    let obj = JSON.parse(JSON.stringify(req.body));
    var bytes  = CryptoJS.AES.decrypt(obj.password, 'budifgbbesiufbeugahuf7yu76yty874y4hr7fdyg3huey7y');
    var originalPass = bytes.toString(CryptoJS.enc.Utf8);
    let hashedPass = await database.getUserPassword(obj.username, "users");
    let passwordMatch = await comparePasswords(originalPass, hashedPass);
    if (passwordMatch) {  
        database.deleteUser(obj.username, "users");
        res.status(200);
        res.send("User deleted");
        console.log("User " + obj.username + " deleted");
    } else {
        res.status(401);
        res.send("Password does not match");
        console.log("Password does not match, user not deleted");
    }  
})

app.use('/api/user/update', async (req, res) => {
    // coming soon
})

app.use('/api/user/captcha', async (req, res) => {
    const captcha = req.body;
    let captchaValue = captcha.captcha;

    const secretKey = require('./config.json');


    await axios.post(
      `https://www.google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${captchaValue}`
      );

      if (res.status(200)) {
        res.send("true");
    }else{
        res.status(401);
      res.send("false");
    }

})

app.post('/api/email/add', async (req, res) => {
    const { apiKey, audienceId } = require('./config.json');

    const email = req.body.email;

    const data = {
        email_address: email,
        status: 'subscribed'
    };

    const url = `https://us1.api.mailchimp.com/3.0/lists/${audienceId}/members`; 

    const headers = {
        'Content-Type': 'application/json',
        'Authorization': `Basic ${Buffer.from(`apikey:${apiKey}`).toString('base64')}`
    };


    axios.post(url, data, { headers })
        .then(response => {
            console.log(response.data);
            // Handle the success response here
        })
        .catch(error => {
            console.error(error.response.data);
            // Handle the error response here
        });

})


  


async function comparePasswords(inputPassword, hashedPassword) {
    const match = await bcrypt.compare(inputPassword, hashedPassword);
    return match;
}


async function genToken(username, password) {
    const token = bcrypt.hash(username + password, 10);
    return token;


}



app.listen(8080, () => console.log('API is running on http://localhost:8080/'));