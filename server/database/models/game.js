let mongoose = require('mongoose');

const gameSchema = new mongoose.Schema({
    round: { type: Number, required: true },
    region: { type: String, required: true },
    gameIndex: { type: Number, required: true },
    team1: { type: mongoose.Schema.Types.ObjectId, ref: 'Team' },
    team2: { type: mongoose.Schema.Types.ObjectId, ref: 'Team' },
    winner: { type: mongoose.Schema.Types.ObjectId, ref: 'Team' },
    loser: { type: mongoose.Schema.Types.ObjectId, ref: 'Team' },
    score: { type: String }
  });

module.exports = mongoose.model('Game', gameSchema);