let mongoose = require('mongoose');
let gameSchema = require('./game.js');

const bracketSchema = new mongoose.Schema({
    name: { type: String, required: true },
    teams: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Team' }],
    games: [gameSchema],
  });

module.exports = mongoose.model('Bracket', bracketSchema);