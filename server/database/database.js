let mongoose = require('mongoose');
const mongoURL = require('./config.json').mongoURL;

const database = 'mm-users';

class Database {
    constructor() {
      this._connect();
    }
  
    _connect() {
      mongoose
        .connect(`mongodb+srv://${mongoURL}/${database}`)
        .then(() => {
          console.log('Database connection successful');
        })
        .catch((err) => {
          console.error('Database connection error');
        });
    }

    async listDocuments(collectionName) {
      const collection = mongoose.connection.collection(collectionName);
      const documents = await collection.find().toArray();
      console.log(documents);
    }

    async findUserByUsername(inputUsername, collectionName) {
      try {
        // Find all documents in the user collection
        const users = await mongoose.connection.collection(collectionName).find().toArray();
    
        // Loop through all documents to check if the input username matches
        for (let i = 0; i < users.length; i++) {
          const user = users[i];
          if (user.username === inputUsername) {
            return true;
          }
        }
    
        // If no match was found, return false
        return false;
      } catch (err) {
        throw err;
      }
    }

    async getUserPassword(username, collectionName) {
      const user = await mongoose.connection.collection(collectionName).findOne({ username });
      if (!user) {
        throw new Error('User not found');
      }
      return user.password;
    }

    async getUserName(username, collectionName) {
      const user = await mongoose.connection.collection(collectionName).findOne({ username });
      if (!user) {
        throw new Error('User not found');
      }
      return user.username;
    }

    async deleteUser(username, collectionName) {
      const user = await mongoose.connection.collection(collectionName).findOneAndDelete({ username });
      if (!user) {
        throw new Error('User not found');
      }
      console.log("User " + username + " deleted");
    }

  }
  
  module.exports = new Database();