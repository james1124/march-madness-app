import React from 'react';
import './NotFound.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';

function NotFound() {
  return (
    <div className="flex flex-col h-screen bg-gray-100">
      <div className="container mx-auto flex-grow flex flex-col justify-center items-center">
        <h1 className="text-4xl font-bold text-red-500 mb-4"><FontAwesomeIcon icon={faExclamationTriangle} className="mr-2" />404 Not Found</h1>
        <p className="text-lg text-gray-700 mb-4">Sorry, the page you are looking for does not exist.</p>
        <a href="/dashboard" className="text-blue-500 hover:text-blue-700">Go back to the homepage</a>
      </div>
    </div>
  );
}

export default NotFound;
