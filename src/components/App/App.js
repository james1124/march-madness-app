// @flow

import React from "react"
import { Route, Routes } from 'react-router-dom';
import useToken from './useToken';
import './App.css';

import Dashboard from '../Dashboard/Dashboard';
import Login from '../Login/Login';
import Register from '../Register/Register';
import Preferences from '../Preferences/Preferences';
import Bracket from '../Bracket/Bracket';
import TermsAndConditions from '../TermsAndConditions/TermsAndConditons';
import NotFound from '../NotFound/NotFound';
import Privacy from '../Privacy/Privacy';


function App() {
  const { token, setToken } = useToken();


/*
  if (window.location.pathname === "/terms") {
    return <Navigate to="/terms" />
  } else if (window.location.pathname === "/privacy") {
    return <Navigate to="/privacy" />
  } 
*/

  switch (window.location.pathname) {
    case "/terms":
      return <TermsAndConditions />
    case "/privacy":
      return <Privacy />
    default:
      break;
  }
   

  if(!token) {
    return <Login setToken={setToken} />  
  } 

  if (token) {
    if (window.location.pathname === "/") {
      // window.location.href = "/dashboard";
      return <Dashboard />
    }
  }

  return (

    <div className="wrapper">
        
        <Routes>
          <Route path="preferences" element={<Preferences />} />
          <Route path="dashboard" element={<Dashboard />} />
          <Route path="register" element={<Register />} />
          <Route path="bracket" element={<Bracket />} />
          <Route path='*' element={<NotFound />}/>
          <Route path="/terms" element={<TermsAndConditions/>} />
          <Route path="/privacy" element={<Privacy/>} />
        </Routes>
      </div>
    );
}

export default App;
