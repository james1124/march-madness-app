import './Dashboard.css'
import Navbar from '../NavBar/NavBar';

export default function Dashboard() {

  const links = [
    {url: '/bracket', text: 'Bracket'},
    {url: '/preferences', text: 'Preferences'}
  ];

  return(
    <div id="wrapper">
      <Navbar links={links} />
      <div className="flex flex-col items-center justify-center h-screen">
        <div className="bg-white rounded-lg shadow-lg p-6">
          <h2 className="text-2xl font-bold mb-4">Dashboard</h2>
          {getGreeting(localStorage.getItem('username'))}
        </div>
      </div>
    </div>
  );
}

function getGreeting(user) {
  if (user === 'username') {
    return <h1>Hello, test user!</h1>;
  } else if (user) {
    return <h1>Hello, {user}!</h1>;
  } 
  return <h1>Hello, Stranger.</h1>;
}
