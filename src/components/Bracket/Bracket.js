import React from 'react';
import Navbar from "../NavBar/NavBar";
import MarchMadnessBracket from './BracketComponet';

export default function Bracket() {
    const links = [
    {url: '/dashboard', text: 'Dashboard'},
    {url: '/preferences', text: 'Preferences'}
    ];
  return (
    <div id="wrapper">
      <Navbar links={links} />
      <MarchMadnessBracket />
    </div>
  );
}


