import React from 'react';
import "./Bracket.css"

const MarchMadnessBracket = () => {
    return (
        <div classNameName="bracket">
            <section>
                <div className='tournament'>
                    <ul className='round seed'>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>
                            <span>1</span>
                            <span>Villanova</span>
                        </li>
                        <li className='game-left game-bottom'>
                            <span>16</span>
                            <span>New Orleans / Mount&hellip;</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>
                            <span>8</span>
                            <span>Wisconsin</span>
                        </li>
                        <li className='game-left game-bottom'>
                            <span>9</span>
                            <span>Virginia Tech</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>
                            <span>5</span>
                            <span>Virginia</span>
                        </li>
                        <li className='game-left game-bottom'>
                            <span>12</span>
                            <span>North Carolina - Wil&hellip;</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>
                            <span>4</span>
                            <span>Florida</span>
                        </li>
                        <li className='game-left game-bottom'>
                            <span>13</span>
                            <span>East Tennessee State</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>
                            <span>6</span>
                            <span>Southern Methodist</span>
                        </li>
                        <li className='game-left game-bottom'>
                            <span>11</span>
                            <span>Southern California &hellip;</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>
                            <span>3</span>
                            <span>Baylor</span>
                        </li>
                        <li className='game-left game-bottom'>
                            <span>14</span>
                            <span>New Mexico State</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>
                            <span>7</span>
                            <span>South Carolina</span>
                        </li>
                        <li className='game-left game-bottom'>
                            <span>10</span>
                            <span>Marquette</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>
                            <span>2</span>
                            <span>Duke</span>
                        </li>
                        <li className='game-left game-bottom'>
                            <span>15</span>
                            <span>Troy</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>
                            <span>1</span>
                            <span>Gonzaga</span>
                        </li>
                        <li className='game-left game-bottom'>
                            <span>16</span>
                            <span>South Dakota State</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>
                            <span>8</span>
                            <span>NorthWestern</span>
                        </li>
                        <li className='game-left game-bottom'>
                            <span>9</span>
                            <span>Vanderbilt</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>
                            <span>5</span>
                            <span>Notre Dame</span>
                        </li>
                        <li className='game-left game-bottom'>
                            <span>12</span>
                            <span>Princeton</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>
                            <span>4</span>
                            <span>West Virginia</span>
                        </li>
                        <li className='game-left game-bottom'>
                            <span>13</span>
                            <span>Bucknell</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>
                            <span>6</span>
                            <span>Maryland</span>
                        </li>
                        <li className='game-left game-bottom'>
                            <span>11</span>
                            <span>Xavier</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>
                            <span>3</span>
                            <span>Florida State</span>
                        </li>
                        <li className='game-left game-bottom'>
                            <span>14</span>
                            <span>FLorida Gulf Coast</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>
                            <span>7</span>
                            <span>Saint Marys</span>
                        </li>
                        <li className='game-left game-bottom'>
                            <span>10</span>
                            <span>Virginia Commonwealt&hellip;</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>
                            <span>2</span>
                            <span>Arizona</span>
                        </li>
                        <li className='game-left game-bottom'>
                            <span>15</span>
                            <span>North Dakota</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                    </ul>
                    <ul className='round round-1'>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>Villanova</li>
                        <li className='game-left spacer'>&nbsp;</li>
                        <li className='game-left game-bottom'>Wisconsin</li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>Virginia</li>
                        <li className='game-left spacer'>&nbsp;</li>
                        <li className='game-left game-bottom'>Florida</li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>Southern California ...</li>
                        <li className='game-left spacer'>&nbsp;</li>
                        <li className='game-left game-bottom'>Baylor</li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>Southern Carolina</li>
                        <li className='game-left spacer'>&nbsp;</li>
                        <li className='game-left game-bottom'>Duke</li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>Gonzaga</li>
                        <li className='game-left spacer'>&nbsp;</li>
                        <li className='game-left game-bottom'>NorthWestern</li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>Notre Dame</li>
                        <li className='game-left spacer'>&nbsp;</li>
                        <li className='game-left game-bottom'>West Virginia</li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>Xavier</li>
                        <li className='game-left spacer'>&nbsp;</li>
                        <li className='game-left game-bottom'>Florida State</li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>Saint Marys</li>
                        <li className='game-left spacer'>&nbsp;</li>
                        <li className='game-left game-bottom'>Arizona</li>
                        <li className='spacer'>&nbsp;</li>
                    </ul>
                    <ul className='round round-2'>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>Wisconsin</li>
                        <li className='game-left spacer'>&nbsp;</li>
                        <li className='game-left game-bottom'>Florida</li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>Baylor</li>
                        <li className='game-left spacer'>&nbsp;</li>
                        <li className='game-left game-bottom'>South Carolina</li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>Gonzaga</li>
                        <li className='game-left spacer'>&nbsp;</li>
                        <li className='game-left game-bottom'>West Virginia</li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>Xavier</li>
                        <li className='game-left spacer'>&nbsp;</li>
                        <li className='game-left game-bottom'>Arizona</li>
                        <li className='spacer'>&nbsp;</li>
                    </ul>
                    <ul className='round round-3'>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>Florida</li>
                        <li className='game-left spacer region region-right'>East</li>
                        <li className='game-left game-bottom'>South Carolina</li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>Gonzaga</li>
                        <li className='game-left spacer region region-right'>West</li>
                        <li className='game-left game-bottom'>Xavier</li>
                        <li className='spacer'>&nbsp;</li>
                    </ul>
                    <ul className='round round-4'>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>South Carolina</li>
                        <li className='game-left spacer'>&nbsp;</li>
                        <li className='game-left game-bottom'>Gonzaga</li>
                        <li className='spacer'>&nbsp;</li>
                    </ul>
                    <ul className='round semi-final'>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-left game-top'>Gonzaga</li>
                        <li className='spacer'>&nbsp;</li>
                    </ul>
                    <ul className='round finals'>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game final'>North Carolina</li>
                        <li className='spacer'>&nbsp;</li>
                    </ul>
                    <ul className='round semi-final'>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>North Carolina</li>
                        <li className='spacer'>&nbsp;</li>
                    </ul>
                    <ul className='round round-4'>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>Oregon</li>
                        <li className='game-right spacer'>&nbsp;</li>
                        <li className='game-right game-bottom'>North Carolina</li>
                        <li className='spacer'>&nbsp;</li>
                    </ul>
                    <ul className='round round-3'>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>Kansas</li>
                        <li className='game-right spacer region region-left'>Midwest</li>
                        <li className='game-right game-bottom'>Oregon</li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>North Carolina</li>
                        <li className='game-right spacer region region-left'>South</li>
                        <li className='game-right game-bottom'>Kentucky</li>
                        <li className='spacer'>&nbsp;</li>
                    </ul>
                    <ul className='round round-2'>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>Kansas</li>
                        <li className='game-right spacer'>&nbsp;</li>
                        <li className='game-right game-bottom'>Purdue</li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>Oregon</li>
                        <li className='game-right spacer'>&nbsp;</li>
                        <li className='game-right game-bottom'>Michigan</li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>North Carolina</li>
                        <li className='game-right spacer'>&nbsp;</li>
                        <li className='game-right game-bottom'>Butler</li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>UCLA</li>
                        <li className='game-right spacer'>&nbsp;</li>
                        <li className='game-right game-bottom'>Kentucky</li>
                        <li className='spacer'>&nbsp;</li>
                    </ul>
                    <ul className='round round-1'>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>Kansas</li>
                        <li className='game-right spacer'>&nbsp;</li>
                        <li className='game-right game-bottom'>Michigan State</li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>Iowa State</li>
                        <li className='game-right spacer'>&nbsp;</li>
                        <li className='game-right game-bottom'>Purdue</li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>Rhode Island</li>
                        <li className='game-right spacer'>&nbsp;</li>
                        <li className='game-right game-bottom'>Oregon</li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>Michigan</li>
                        <li className='game-right spacer'>&nbsp;</li>
                        <li className='game-right game-bottom'>Louisville</li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>North Carolina</li>
                        <li className='game-right spacer'>&nbsp;</li>
                        <li className='game-right game-bottom'>Arkansas</li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>Middle Tennessee Sta&hellip;</li>
                        <li className='game-right spacer'>&nbsp;</li>
                        <li className='game-right game-bottom'>Butler</li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>Cincinnati</li>
                        <li className='game-right spacer'>&nbsp;</li>
                        <li className='game-right game-bottom'>UCLA</li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>Wichita State</li>
                        <li className='game-right spacer'>&nbsp;</li>
                        <li className='game-right game-bottom'>Kentucky</li>
                        <li className='spacer'>&nbsp;</li>
                    </ul>
                    <ul className='round seed'>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>
                            <span>Kansas</span>
                            <span>1</span>
                        </li>
                        <li className='game-right game-bottom'>
                            <span>UC Davis / North Car&hellip;</span>
                            <span>16</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>
                            <span>Miami</span>
                            <span>8</span>
                        </li>
                        <li className='game-right game-bottom'>
                            <span>Michigan State</span>
                            <span>9</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>
                            <span>Iowa State</span>
                            <span>5</span>
                        </li>
                        <li className='game-right game-bottom'>
                            <span>Nevada</span>
                            <span>12</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>
                            <span>Purdue</span>
                            <span>4</span>
                        </li>
                        <li className='game-right game-bottom'>
                            <span>Vermont</span>
                            <span>13</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>
                            <span>Creighton</span>
                            <span>6</span>
                        </li>
                        <li className='game-right game-bottom'>
                            <span>Rhode Island</span>
                            <span>11</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>
                            <span>Oregon</span>
                            <span>3</span>
                        </li>
                        <li className='game-right game-bottom'>
                            <span>Iona</span>
                            <span>14</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>
                            <span>Michigan</span>
                            <span>7</span>
                        </li>
                        <li className='game-right game-bottom'>
                            <span>Oklahoma State</span>
                            <span>10</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>
                            <span>Louisville</span>
                            <span>2</span>
                        </li>
                        <li className='game-right game-bottom'>
                            <span>Jacksonville State</span>
                            <span>15</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>
                            <span>North Carolina</span>
                            <span>1</span>
                        </li>
                        <li className='game-right game-bottom'>
                            <span>Texas Southern</span>
                            <span>16</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>
                            <span>Arkansas</span>
                            <span>8</span>
                        </li>
                        <li className='game-right game-bottom'>
                            <span>Seton Hall</span>
                            <span>9</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>
                            <span>Minnesota</span>
                            <span>5</span>
                        </li>
                        <li className='game-right game-bottom'>
                            <span>Middle Tennessee Sta&hellip;</span>
                            <span>12</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>
                            <span>Butler</span>
                            <span>4</span>
                        </li>
                        <li className='game-right game-bottom'>
                            <span>Winthrop</span>
                            <span>13</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>
                            <span>Cincinnati</span>
                            <span>6</span>
                        </li>
                        <li className='game-right game-bottom'>
                            <span>Wake Forest / Kansas&hellip;</span>
                            <span>11</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>
                            <span>UCLA</span>
                            <span>3</span>
                        </li>
                        <li className='game-right game-bottom'>
                            <span>Kent State</span>
                            <span>14</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>
                            <span>Dayton</span>
                            <span>7</span>
                        </li>
                        <li className='game-right game-bottom'>
                            <span>Wichita State</span>
                            <span>10</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                        <li className='game-right game-top'>
                            <span>Kentucky</span>
                            <span>2</span>
                        </li>
                        <li className='game-right game-bottom'>
                            <span>Northern Kentucky</span>
                            <span>15</span>
                        </li>
                        <li className='spacer'>&nbsp;</li>
                    </ul>
                </div>
            </section>
        </div>

    );
}

export default MarchMadnessBracket;

