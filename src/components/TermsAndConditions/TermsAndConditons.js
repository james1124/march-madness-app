import React from 'react';
import './TermsAndConditions.css';
import Contact from '../Addons/Contact';

function TermsAndConditions() {
    return (
        <div className="container mx-auto px-4 py-8">
            <h1 className="text-4xl font-bold mb-4">Terms and Conditions</h1>
            <div className="bg-white rounded-lg shadow-md p-8">
            <h3 className="text-2xl font-bold mb-4">User Accounts</h3>
                1.1 In order to use our app, you must create a user account.
                <br></br>
                1.2 You are responsible for maintaining the confidentiality of your account information, including your username and password.
                <br></br>
                1.3 You agree to notify us immediately of any unauthorized access or use of your account.
                <br></br>
                1.4 We reserve the right to terminate or suspend your account at any time for any reason without notice.

                <h3 className="text-2xl font-bold mb-4">Data Storage</h3>
                2.1 Our app allows you to store your emails in our system.
                <br></br>
                2.2 We use industry-standard encryption to protect your data.
                <br></br>
                2.3 We do not sell or share your data with third parties.
                <br></br>
                2.4 You agree to comply with all applicable laws and regulations when using our app.
                <br></br>
                2.5 You acknowledge that we have no control over the content of your emails and we are not responsible for any damages or losses caused by the content of your emails.

                <h3 className="text-2xl font-bold mb-4">Deletion of User Accounts and Data</h3>
                3.1 You may delete your user account and all associated data at any time.
                <br></br>
                3.2 We may retain your data for a reasonable period of time for backup, archival, or audit purposes.
                <br></br>
                3.3 We reserve the right to delete your account and data if we believe that you have violated these terms and conditions.

                <h3 className="text-2xl font-bold mb-4">Access to Your Data</h3>
                4.1 You may request a copy of your data at any time.
                <br></br>
                4.2 We will provide you with a copy of your data within a reasonable period of time.

                <h3 className="text-2xl font-bold mb-4">Intellectual Property</h3>
                5.1 Our app and all content and materials contained in it are owned by us or our licensors.
                <br></br>
                5.2 You may not copy, modify, distribute, sell, or create derivative works based on our app or any content or materials contained in it without our prior written consent.

                <h3 className="text-2xl font-bold mb-4">Liability</h3>
                6.1 We are not liable for any direct, indirect, incidental, consequential, or punitive damages arising out of or in connection with your use of our app.
                <br></br>
                6.2 We are not liable for any damages caused by events beyond our reasonable control, including without limitation, acts of God, war, terrorism, riots, or other natural disasters.

                <h3 className="text-2xl font-bold mb-4">Modifications to these Terms and Conditions</h3>
                7.1 We reserve the right to modify these terms and conditions at any time without notice.
                <br></br>
                7.2 Your continued use of our app after any modifications to these terms and conditions shall constitute your acceptance of the modified terms.

                <h3 className="text-2xl font-bold mb-4">Governing Law</h3>
                8.1 These terms and conditions shall be governed by and construed in accordance with the laws of the United States of America.
                <br></br>
                8.2 Any disputes arising out of or in connection with these terms and conditions shall be resolved in accordance with the dispute resolution procedures set forth in these terms.

                <h3 className="text-2xl font-bold mb-4">Contact</h3>
                If you have any questions or concerns about these terms and conditions, please contact us via the methods listed below. <Contact></Contact>
            </div>
        </div>
    );
}

export default TermsAndConditions;
