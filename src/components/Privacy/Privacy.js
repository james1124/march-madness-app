import React, { useState } from "react";
export default function Privacy() {

    const [visible, setVisible] = useState(true);
        
    function dismiss() {
        setVisible(false);
        console.log(visible);
      }
  
      
    return (
        <div> { /* TODO: Make reusable via addons folder */}
        <div class="fixed top-0 left-0 right-0 bottom-0 bg-gray-900 opacity-80"></div>
            <div className="max-w-5xl mx-auto px-4 md:px-8 fixed top-0 left-0 right-0 bottom-0 z-50 flex items-center justify-center">
                
            <div className="flex justify-between p-4 rounded-md bg-gray-400 border border-blue-300">
                <div className="flex gap-3">
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-blue-500" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                            <path strokeLinecap="round" strokeLinejoin="round" d="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                    </div>
                    <div className="self-center">
                        <span className="text-blue-600 font-medium">
                            New update available
                        </span>
                        <div className="text-blue-600">
                            <p className="mt-2 sm:text-sm">
                                yeah i know, this page looks awful, i'm working on it
                            </p>
                            <div className="mt-2">
                                <button
                                    id="dismiss"
                                    onClick={dismiss}
                                    className="inline-flex items-center font-medium hover:underline sm:text-sm">
                                    i dont jsaffjfa know how to call the state hook's setter function to update the state and re-render:
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-3.5 w-3.5 ml-1" viewBox="0 0 20 20" fill="currentColor">
                                        <path fillRule="evenodd" d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z" clipRule="evenodd" />
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            Privacy Policy for Email and User Name Storage App

Thank you for using our app. We take your privacy very seriously, and this privacy policy explains how we collect, use, and protect your personal information.

Information We Collect

We only collect the information necessary to provide you with our service. When you register for our app, we collect your email and username. We do not collect or store your password, as it is hashed and stored securely on our servers. We also collect information about how you use our app, such as your device type and operating system.

How We Use Your Information

We use your information to provide you with our service and to improve our app. We may use your email to send you notifications about new features, updates, and other important information related to our app.

We may also use your email to send marketing emails, such as newsletters or promotional offers. In order to do this, we store your email address in a database and send it to an external service, such as Mailchimp. You may opt-out of receiving marketing emails at any time by following the instructions in the email.

We do not share your information with third parties, except as necessary to provide our service or as required by law. We may share your information with our service providers, such as hosting and email service providers, but only to the extent necessary to provide our service.

Data Retention and Deletion

You may delete your account at any time. When you delete your account, we will delete all of your personal information from our servers, except as required by law. We may retain anonymous or aggregated data for analytical purposes.

Security

We take appropriate measures to protect your personal information from unauthorized access, disclosure, or destruction. We use industry-standard security measures, such as encryption and firewalls, to protect your information.

Changes to this Privacy Policy

We may update this privacy policy from time to time. If we make any material changes, we will notify you by email or through our app. Your continued use of our app after any changes to this privacy policy indicates your acceptance of the changes.

Contact Us

If you have any questions or concerns about this privacy policy, please contact us at [email address].
        </div>
    )
}