import React, { Suspense } from 'react';
import { useImage } from 'react-image';
import styled from 'styled-components';
import "./NavBar.css"

const Navbar = ({ links }) => {
  const avatarUrl = `https://robohash.org/${localStorage.getItem("username")}?set=set4`;

  const handleLogout = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("username");
    window.location.reload();
  };

  return (
    <nav className="bg-gray-800 rounded-lg">
      <div className="mx-auto px-4 sm:px-6 lg:px-8">
        <div className="flex justify-between h-16">
          <div className="flex">
            <div className="flex-shrink-0 flex items-center">
              <Suspense fallback={<div>Loading...</div>}>
                <ProfileImageComponent src={avatarUrl} />
              </Suspense>
              <span className="text-white ml-2 text-xl font-semibold">March Madness App</span>
            </div>
          </div>
          <div className="flex items-center" id='test'>
            {links.map((link, index) => (
              <NavLink key={index} href={link.url}>
                <Button>{link.text}</Button>
              </NavLink>
            ))}
            <RedButton onClick={handleLogout}>Logout</RedButton>
          </div>
        </div>
      </div>
    </nav>
  );
};

const NavLink = styled.a`
  text-decoration: none;
  color: white;
  &:hover {
    color: gray;
  }
`;

function ProfileImageComponent({ src }) {
  const { src: imgSrc } = useImage({
    srcList: src,
  });

  return <img className="profile_image" alt="profile_image" src={imgSrc} />;
}

const RedButton = styled.button`
  background-color: red;
  color: black;
  font-size: 20px;
  padding: 10px 60px;
  border-radius: 5px;
  margin: 10px 0px;
  cursor: pointer;
  &:disabled {
    color: grey;
    opacity: 0.7;
    cursor: default;
  }
`;

const Button = styled.button`
  background-color: black;
  color: white;
  font-size: 20px;
  padding: 10px 60px;
  border-radius: 5px;
  margin: 10px 0px;
  cursor: pointer;
  &:disabled {
    color: grey;
    opacity: 0.7;
    cursor: default;
  }
`;

export default Navbar;
