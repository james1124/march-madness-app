import React from 'react';
import { useForm } from "react-hook-form";
import bcrypt from 'bcryptjs-react';
import './Register.css';
import axios from 'axios';
import ReCAPTCHA from "react-google-recaptcha";


export default function Register() {

  function onChange(value) {
    //console.log('Captcha value:', value);
    console.log("Captcha submitted");
  }

  const onSubmit = async (data) => {

    const recaptcha = localStorage.getItem('_grecaptcha');
    if (recaptcha === null) {
      alert('Please verify that you are not a robot');
      return;
    }


    const isHuman = async () => {
      try {
        const response = await axios.post('http://localhost:8080/api/user/captcha', {
          recaptcha: recaptcha
        });
        return response.data;
      } catch (error) {
        console.error(error);
      }
    };

    const human = await isHuman();
    
    if (human === false) {
      alert('Please verify that you are not a robot');
      return;
    } else {
      console.log('Human verified');
    }

    await registerUser(data);



  document.getElementById("register-form").reset();
};


const { register, handleSubmit, formState: { errors } } = useForm();
// const onSubmit = data => registerUser(data);

return (
  <div className="register-wrapper bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 border-2 border-gray-200">
    <h1 className="text-3xl font-bold mb-4">Register</h1>
    <form onSubmit={handleSubmit(onSubmit)} className="space-y-4" id="register-form">
      <div>
        <label htmlFor="username" className="block font-medium text-gray-700 mb-2">Username</label>
        <input type="text" {...register("username", { required: true })}
          className="border border-gray-300 rounded-lg w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:border-blue-500"
        />
        {errors.username && <span className="text-red-500 text-sm mt-1">This field is required</span>}
      </div>
      <div>
        <label htmlFor="password" className="block font-medium text-gray-700 mb-2">Password</label>
        <input type="password" {...register("password", { required: true })}
          className="border border-gray-300 rounded-lg w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:border-blue-500"
        />
        {errors.password && <span className="text-red-500 text-sm mt-1">This field is required</span>}
      </div>
      <div>
        <label htmlFor="email" className="block font-medium text-gray-700 mb-2">Email</label>
        <input type="email" {...register("email", { required: true })}
          className="border border-gray-300 rounded-lg w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:border-blue-500"
        />
        <p id="helper-text-explanation" className="mt-2 text-sm text-gray-500 dark:text-gray-400">We’ll never share your details. Read our <a href="/privacy" className="font-medium text-blue-600 hover:underline dark:text-blue-500">Privacy Policy</a>.</p>
        {errors.email && <span className="text-red-500 text-sm mt-1">This field is required</span>}
      </div>

      <div className="flex items-center">
        <input {...register("consent", { required: true })} id="consent" type="checkbox" value="" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600" />
        <label htmlFor="link-checkbox" className="ml-2 text-sm font-medium text-gray-900 dark:text-gray-700">I agree with the <a href="/terms" className="text-blue-600 dark:text-blue-500 hover:underline">Terms and Conditions</a>.</label>
      </div>

      <ReCAPTCHA
        sitekey="6LcTovskAAAAAPPBR8mo5zZsnAMDXEBBW8SA4BOv"
        onChange={onChange}
      />
      <button type="submit" className="bg-blue-500 text-white py-2 px-4 rounded hover:bg-blue-600 focus:outline-none focus:bg-blue-600">Register</button>
    </form>

  </div>

);
}
/*
async function registerUser(credentials) {


   console.log(await checkUsername(credentials.username));



  return fetch('http://localhost:8080/register', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ email: credentials.email, username: credentials.username, password: await bcrypt.hash(credentials.password, 10) })
  })
    .then(data => data.json())
}

*/

async function registerUser(credentials) {
  const usernameExists = await checkUsername(credentials.username);
  console.log(usernameExists.exists);
  if (usernameExists.exists) {
    alert("Username already exists");
    return false;
  }

  return fetch('http://localhost:8080/api/user/register', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ email: credentials.email, username: credentials.username, password: await bcrypt.hash(credentials.password, 10) })
  })
    .then(async data => data.json(
      alert("Registration successful"),
      await addEmail(credentials.email)
    ))
}


async function checkUsername(username) {
  return fetch('http://localhost:8080/api/username', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ username: username })
  })
    .then(data => data.json())
}

async function addEmail(email) {
  return fetch('http://localhost:8080/api/email/add', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ email: email })
  })
    .then(data => data.json())
}
