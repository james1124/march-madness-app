import React from 'react';
import './Banner.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';

function Contact() {
  return (
    <div className="container mx-auto px-4 py-8">
      <p className="mb-4">Email: <a href="mailto:ejames25@sfprep.org" className="text-blue-500 hover:text-blue-700"><FontAwesomeIcon icon={faEnvelope} className="mr-2" />ejames25@sfprep.org</a></p>
    </div>
  );
}

export default Contact;
