import React from "react";
import Navbar from "../NavBar/NavBar";
import DeleteAccountForm from "../DeleteUser/DeleteUser";

export default function Preferences() {


  const handleSubmit = async e => {
    e.preventDefault();
    const username = localStorage.getItem('username');

    const formData = new FormData(e.target);
    const data = Object.fromEntries(formData.entries());

    for (const key in data) {
      if (data[key] === "") {
        delete data[key];
      }
    }

    if (data.username === username) {
      delete data.username;
    }

    if (data.password !== data.confirm_password) {
      alert("Passwords do not match");
      return;
    }

    delete data.confirm_password;

    alert("New data: " + JSON.stringify(data));

    /*
      TODO: 
        - Send data to backend
        - Handle errors
        - Handle success
    */




    console.log(data);

  }



  const links = [
    {url: '/dashboard', text: 'Dashboard'},
    {url: '/bracket', text: 'Bracket'},
  ];
  return (
    <div id="wrapper">
      <Navbar links={links} />
      <div className="flex flex-col items-center justify-center h-screen mt-16">
        <div className="bg-white rounded-lg shadow-lg p-6">
          <h2 className="text-2xl font-bold mb-4">Account Settings</h2>
          <form id="preferences_form" onSubmit={handleSubmit}>
            <label className="block mb-2">New Username</label>
            <input
              type="text"
              name="username"
              placeholder="Username"
              className="block mb-4 border border-gray-400 p-2 rounded-lg w-full"
            />
            <label className="block mb-2">New Email</label>
            <input
              type="text"
              name="email"
              placeholder="Email"
              className="block mb-4 border border-gray-400 p-2 rounded-lg w-full"
            />
            <label className="block mb-2">New Password</label>
            <input
              type="password"
              name="password"
              placeholder="Password"
              className="block mb-4 border border-gray-400 p-2 rounded-lg w-full"
            />
            <label className="block mb-2">Confirm New Password</label>
            <input
              type="password"
              name="confirm_password"
              placeholder="Confirm Password"
              className="block mb-4 border border-gray-400 p-2 rounded-lg w-full"
            />
            <button
              type="submit"
              className="bg-blue-500 text-white py-2 px-4 rounded-lg hover:bg-blue-600"
            >
              Submit
            </button>
          </form>
        </div>
        <DeleteAccountForm />
      </div>
    </div>
  );
}
