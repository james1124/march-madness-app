import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Register from '../Register/Register';
import CryptoJS from 'crypto-js';
import axios from 'axios';
import Banner from '../Addons/Banner';

import './Login.css';
import { Navigate } from 'react-router-dom';




async function loginUser(credentials) {

        // Yes I know this is bad, it will be fixed in the future

        let cipherPass = CryptoJS.AES.encrypt(credentials.password, 'budifgbbesiufbeugahuf7yu76yty874y4hr7fdyg3huey7y').toString();

        // console.log(cipherPass);

        const params = new URLSearchParams();
        params.append('username', credentials.username);
        params.append('password', cipherPass);

        return axios.post('http://localhost:8080/api/user/login', params)
          .then(function (data) {
            return data.data;
          })
          .catch(function (error) {
            console.log(error);
          });
      
}

export default function Login({ setToken }) {
    const [username, setUserName] = useState();
    const [password, setPassword] = useState();

    const handleSubmit = async e => {
        e.preventDefault();
        localStorage.setItem('username', username);
        const token = await loginUser({
          username,
          password
        });
        
        if (token.token) {
            setToken(token);
            console.log(token.token);
            <Navigate to="/dashboard" />
        } else {
            alert("Invalid username or password")
        }

    }
    const bannerLinks = [
      {mainText: 'This project is under active development. Please report any bugs or issues on'},
      {highlightText: 'GitLab'},
      {link: 'https://gitlab.com/james1124/march-madness-app/-/issues'}
    ];

    return(
      <div>
        
        <Banner text={bannerLinks}/>
      
        <div className="flex flex-col items-center justify-center h-screen">
  <h1 className="text-3xl font-bold mb-4 pt-20">Please Log In</h1>
  <p className="mb-4">Test credentials: Username: <strong>username</strong> Password: <strong>password</strong></p>
  <form onSubmit={handleSubmit} className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 border-2 border-gray-200" > 
  <h1 className="text-3xl font-bold mb-4">Login</h1>
    <div className="mb-4">
      <label className="block text-gray-700 font-bold mb-2" htmlFor="username">
        Username
      </label>
      <input
        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
        id="username"
        type="text"
        placeholder="Username"
        onChange={e => setUserName(e.target.value)}
      />
    </div>
    <div className="mb-6">
      <label className="block text-gray-700 font-bold mb-2" htmlFor="password">
        Password
      </label>
      <input
        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
        id="password"
        type="password"
        placeholder="Password"
        onChange={e => setPassword(e.target.value)}
      />
    </div>
    <div className="flex items-center justify-between">
      <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
        Submit
      </button>
    </div>
  </form>
  <Register />

</div>
      </div>
  )
}

Login.propTypes = {
    setToken: PropTypes.func.isRequired
  }