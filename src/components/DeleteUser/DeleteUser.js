import React, { useState } from 'react';
import { Navigate } from 'react-router-dom';
import axios from 'axios';
import CryptoJS from 'crypto-js';

function DeleteAccountForm() {

    const [password, setPassword] = useState('');

    const handleSubmit = async(event) => {
        event.preventDefault();
        let encryptedString = CryptoJS.AES.encrypt(password, 'budifgbbesiufbeugahuf7yu76yty874y4hr7fdyg3huey7y').toString();

        axios.delete('http://localhost:8080/api/user/delete', {
            data: {
                password: encryptedString,
                username: localStorage.getItem('username')
            }
        }).then((response) => {
            console.log(response);
            if (response.data === "User deleted") {
                localStorage.removeItem('username');
                localStorage.removeItem('token');
                <Navigate to="/login" />
            }
        }).catch((err) => {
            console.error(err);
        }
        );
        

    }

    return (
        <div className="max-w-md mx-auto mt-10 bg-white rounded-lg shadow-lg p-6">
            <form onSubmit={handleSubmit}>
                <h2 className="text-3xl font-bold mb-5">Delete Account</h2>
                <div className="mb-4">
                    <input type="password" name="password" id="password" placeholder="Enter your password"
                        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                        value={password} onChange={(event) => setPassword(event.target.value)} required />
                </div>
                <button type="submit" className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded">
                    Delete Account
                </button>
            </form>
        </div>
    );
}

export default DeleteAccountForm;
