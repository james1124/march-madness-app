# March Madness 2023 Bracket Application

---
The goal for this project is to create a simple application that allows users to create a bracket for the 2023 March Madness tournament. Users can create an account, log in, and create a bracket. Users can also view other users' brackets and see how they did in the tournament.(maybe lmao)

---

#### Technologies used:

- [ReactJS](https://reactjs.org/)
- [MongoDB](https://www.mongodb.com/)
- [NodeJS](https://nodejs.org/en/)
- [bcrypt](https://www.npmjs.com/package/bcrypt) and [crypto-js](https://www.npmjs.com/package/crypto-js)
- [Google reCAPTCHA](https://www.google.com/recaptcha/about/)
- [Tailwind CSS](https://tailwindcss.com/)
- [ExpressJS](https://expressjs.com/)
- [Axios](https://axios-http.com/)
- [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken)

---

### Login Page(working on it)

![login](https://i.imgur.com/71UoPOR.png)

### Dashboard

![Dashboard](https://i.imgur.com/nNVyS5I.png)

### Preferences

![Preferences](https://i.imgur.com/GrNxvHI.png)

### Bracket

![bracket](https://i.imgur.com/xPyvSEJ.png)

## Table of Contents



- [Getting Started](#getting-started)
- [Prerequisites](#prerequisites)
- [Installing](#installing)


# Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

# Prerequisites

What things you need to install the software and how to install them

1. Node.js
2. A MongoDB Server

# Installing

A step by step series of examples that tell you how to get a development env running

step

1. Clone this repo with `git clone https://gitlab.com/james1124/march-madness-app.git`
2. Install the required packages by running `npm i` inside of the project folder
3. Start the server by entering into the `server` folder and running `node server.js`
4. Start the web application by running `npm run start` in the project root
5. Open [localhost:3000](http://localhost:3000)!

---

This project consists of two main sections, a front-end web app written in pure React and a backend server running Express. 

The backend handles things like user registration, user data applications and passwording checking. It also communicates with the MongoDB server.

The fontend handles user interaction and communicates with the backend via a RESTful API. This allows the frontend to retreive information like user emails and do things such as update the database.  


### Security

While its obvious that this app is full of janky, insecure code, it does have some security features:

- Password Hashing
    - Passwords in the database are hashed and unreadable by me or anyone else. ![passwords](https://i.imgur.com/S0QW04y.png)
- All user operations take place serverside and messages between the browser and the server and encrypted using public key cryptography




